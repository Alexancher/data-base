#include "Bike.h"

char* serialize_bike(Bike* bike) {
	null_check(bike);

	char* str = (char*)calloc(sizeof(char), 256);
	sprintf(str, "%s %d %s", bike->name, bike->wheels, bike->engine);

	return str;
}

Bike* deserialize_bike(char* str) {
	null_check(str);

	Bike* bike = (Bike*)malloc(sizeof(Bike));
	int i = 0;
	int len = strlen(str);
	char* name = (char*)calloc(sizeof(char), 256);
	char* engine = (char*)calloc(sizeof(char), 256);

	while (i < len && str[i] != 32) {
		name[i] = str[i];
		i++;
	}

	i++;
	int wheels = 0;
	while (i < len && str[i] != 32) {
		wheels *= 10;
		wheels += str[i] - '0';
		i++;
	}

	i++;
	int idx = 0;
	while (str[i] != '\n' && i < len) {
		engine[idx] = str[i];
		idx++;
		i++;
	}

	bike->name = name;
	bike->wheels = wheels;
	bike->engine = engine;

	return bike;
}
