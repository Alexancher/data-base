#include "Scooter.h"

char* serialize_scooter(Scooter* scooter) {
	null_check(scooter);

	char* str = (char*)calloc(sizeof(char), 256);
	sprintf(str, "%d %d", scooter->size, scooter->price);

	return str;
}

Scooter* deserialize_scooter(char* str) {
	null_check(str);

	Scooter* scooter = (Scooter*)malloc(sizeof(Scooter));
	int len = strlen(str);
	int size = 0;
	int i = 0;
	while (i < len && str[i] != 32) {
		size *= 10;
		size += str[i] - '0';
		i++;
	}

	i++;
	int price = 0;
	while (i < len && str[i] != '\n') {
		price *= 10;
		price += str[i] - '0';
		i++;
	}

	scooter->size = size;
	scooter->price = price;

	return scooter;
}