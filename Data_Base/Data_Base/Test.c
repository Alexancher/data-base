#include "Test.h"

void test() {
	list* test_base = (list*)malloc(sizeof(list));
	test_base = create_list(test_base);
	test_base->name = "test_input";

	printf("Loading base...\n");
	load_base(test_base);
	if (test_base->head == NULL) {
		printf("Base is empty. Aborting\n");
		assert(0);
	}

	test_base->name = "test_output";
	printf("Saving the base...\n");
	save_base(test_base);

	printf("Showing base\n");
	show_base(test_base);

	printf("Adding element...\n");
	int type = 0;
	printf("Enter the type\n");
	scanf("%d", &type);
	char* str = (char*)calloc(sizeof(char), 256);
	char* name = (char*)calloc(sizeof(char), 256);
	int wheels = 0;
	char* brand = (char*)calloc(sizeof(char), 256);
	char* model = (char*)calloc(sizeof(char), 256);

	switch (type) {
	case BIKE_ID:
		printf("Enter the name\n");
		scanf("%s", name);
		printf("Enter the number of wheels\n");
		scanf("%d", &wheels);
		printf("Enter the engine\n");
		char* engine = (char*)calloc(sizeof(char), 256);
		scanf("%s", engine);
		sprintf(str, "%s %d %s", name, wheels, engine);
		break;

	case BICYCLE_ID:
		printf("Enter the name\n");
		scanf("%s", name);
		printf("Enter the number of wheels\n");
		scanf("%d", &wheels);
		sprintf(str, "%s %d", name, wheels);
		break;

	case CAR_ID:
		printf("Enter the brand\n");
		scanf("%s", brand);
		printf("Enter the model\n");
		scanf("%s", model);
		sprintf(str, "%s %s", brand, model);
		break;

	case SPORTCAR_ID:
		printf("Enter the brand\n");
		scanf("%s", brand);
		printf("Enter the model\n");
		scanf("%s", model);
		printf("Enter the top speed\n");
		int top_speed = 0;
		scanf("%d", &top_speed);
		sprintf(str, "%s %s %d", brand, model, top_speed);
		break;

	case SCOOTER_ID:
		printf("Enter the size\n");
		int size = 0;
		scanf("%d", &size);
		printf("Enter the price\n");
		int price = 0;
		scanf("%d", &price);
		sprintf(str, "%d %d", size, price);
		break;
	}

	item* new_element = (item*)malloc(sizeof(item));
	new_element = deserialize_element(type, str);
	add_item(test_base, new_element);
	show_base(test_base);

	printf("Removing element...\n");
	int id = 0;
	printf("Enter ID of the element\n");
	scanf("%d", &id);
	remove_item(test_base, id);
	show_base(test_base);

	printf("get_data check...\n");
	list* data_list = (list*)malloc(sizeof(list));
	data_list = get_data(test_base);
	show_base(data_list);

	printf("find_by_type check...\n");
	type = 0;
	printf("Enter the type\n");
	scanf("%d", &type);
	list* filtered = (list*)malloc(sizeof(list));
	filtered = find_by_type(data_list, type);
	show_base(filtered);

	printf("DONE!");
	getch();
}
