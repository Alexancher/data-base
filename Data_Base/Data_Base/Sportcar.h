#pragma once

#include "Types.h"

#define SPORTCAR_ID 04

typedef struct Sportcar {
	char* brand;
	char* model;
	int top_speed;
} Sportcar;

char* serialize_sportcar(Sportcar* sportcar);

Sportcar* deserialize_sportcar(char* str);
