#pragma once

#include "Types.h"

#define BIKE_ID 01

typedef struct Bike {
	char* name;
	int wheels;
	char* engine;
} Bike;

char* serialize_bike(Bike* bike);

Bike* deserialize_bike(char* str);
