#include "List.h"

list* create_list(list* new_list) {
	new_list->head = NULL;
	new_list->next_id = 1;
	new_list->name = (char*)calloc(sizeof(char), 256);

	return new_list;
}

void add_item(list* List, item* new_item) {
	null_check(new_item);

	item* Item = (item*)malloc(sizeof(item));
	Item->data = new_item->data;
	Item->type = new_item->type;

	Item->id = List->next_id;
	Item->next = List->head;
	List->head = Item;
	List->next_id++;
}

item* find_item(list* List, int id) {
	null_check(List);

	item* item_u_r_looking_for = List->head;
	while (item_u_r_looking_for->id != id && item_u_r_looking_for->next != NULL) {
		item_u_r_looking_for = item_u_r_looking_for->next;
	}

	if (item_u_r_looking_for->id != id) {
		return NULL;
	}

	return item_u_r_looking_for;
}

item* find_prev(list* List, int id) {
	null_check(List);

	item* item_u_r_looking_for = List->head;
	while (item_u_r_looking_for->next->id != id && item_u_r_looking_for->next != NULL) {
		item_u_r_looking_for = item_u_r_looking_for->next;
	}

	if (item_u_r_looking_for->next->id != id) {
		return NULL;
	}

	return item_u_r_looking_for;
}

void remove_item(list* List, int id) {
	item* item_to_remove = find_item(List, id);

	if (item_to_remove == List->head) {
		List->head = List->head->next;
	}
	else {
		item* prev_item = find_prev(List, id);
		prev_item->next = item_to_remove->next;
	}

	free(item_to_remove->data);
	free(item_to_remove);
}

void clean_list(list* List) {
	while (List->head != NULL) {
		remove_item(List, 0);
	}
}

void delete_list(list* List) {
	clean_list(List);
	free(List);
}

void add_to_the_end(list* List, item* new_item) {
	null_check(new_item);

	item* buf = (item*)malloc(sizeof(item));
	buf = List->head;

	item* Item = (item*)malloc(sizeof(item));
	Item->data = new_item->data;
	Item->type = new_item->type;
	
	if (buf == NULL) {
		add_item(List, Item);
		return;
	}

	while (buf->next != NULL) {
		buf = buf->next;
	}

	Item->id = List->next_id;
	List->next_id++;
	Item->next = NULL;
	buf->next = Item;
}

list* find_by_type(list* List, int type) {
	null_check(List);

	list* filtered = (list*)malloc(sizeof(list));
	filtered = create_list(filtered);
	int id = 1;
	item* Item = (item*)malloc(sizeof(item));

	while (id < List->next_id) {
		Item = find_item(List, id);
		if (Item != NULL && Item->type == type) {
			add_item(filtered, Item);
		}
		id++;
	}

	return filtered;
}
