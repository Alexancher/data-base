#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define null_check(X) assert(X != NULL)

typedef struct item {
	void* data;
	struct item* next;
	int id;
	int type;
} item;

typedef struct list {
	item* head;
	int next_id;
	char* name;
} list;

list* create_list(list* new_ist);

void add_item(list* List, item* new_item);

item* find_item(list* List, int id);

item* find_prev(list* List, int id);

void remove_item(list* List, int id);

void clean_list(list* List);

void delete_list(list* List);

void add_to_the_end(list* List, item* new_item);

list* find_by_type(list* List, int type);
