#pragma once

#include "List.h"
#include "Bike.h"
#include "Bicycle.h"
#include "Car.h"
#include "Sportcar.h"
#include "Scooter.h"

list* create_base(list* base);

void add_element(list* base, item* element);

void show_base(list* base);

void save_base(list* base);

void load_base(list* base);

list* get_data(list* base);

void clean_base(list* base);

void free_base(list* base);

item* find_element_by_id(list* base, int id);

list* find_elements_by_type(list* base, int type);

void add_element_to_the_end(list* base, item* element);

void delete_element(list* base, int id);
