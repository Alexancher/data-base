#include "Bicycle.h"

char* serialize_bicycle(Bicycle* bicycle) {
	null_check(bicycle);

	char* str = (char*)calloc(sizeof(char), 256);
	sprintf(str, "%s %d", bicycle->name, bicycle->wheels);

	return str;
}

Bicycle* deserialize_bicycle(char* str) {
	null_check(str);

	Bicycle* bicycle = (Bicycle*)malloc(sizeof(Bicycle));
	int i = 0;
	int len = strlen(str);
	char* name = (char*)calloc(sizeof(char), 256);

	while (i < len && str[i] != 32) {
		name[i] = str[i];
		i++;
	}

	i++;
	int wheels = 0;
	while (i < len && str[i] != '\n') {
		wheels *= 10;
		wheels += str[i] - '0';
		i++;
	}

	bicycle->name = name;
	bicycle->wheels = wheels;

	return bicycle;
}
