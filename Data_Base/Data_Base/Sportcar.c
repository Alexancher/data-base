#include "Sportcar.h"

char* serialize_sportcar(Sportcar* sportcar) {
	null_check(sportcar);

	char* str = (char*)calloc(sizeof(char), 256);
	sprintf(str, "%s %s %d", sportcar->brand, sportcar->model, sportcar->top_speed);

	return str;
}

Sportcar* deserialize_sportcar(char* str) {
	null_check(str);

	Sportcar* sportcar = (Sportcar*)malloc(sizeof(Sportcar));
	int i = 0;
	int len = strlen(str);
	char* brand = (char*)calloc(sizeof(char), 256);
	char* model = (char*)calloc(sizeof(char), 256);

	while (i < len && str[i] != 32) {
		brand[i] = str[i];
		i++;
	}

	i++;
	int idx = 0;
	while (i < len && str[i] != 32) {
		model[idx] = str[i];
		idx++;
		i++;
	}

	i++;
	int top_speed = 0;
	while (str[i] != '\n' && i < len) {
		top_speed *= 10;
		top_speed += str[i] - '0';
		i++;
	}

	sportcar->brand = brand;
	sportcar->model = model;
	sportcar->top_speed = top_speed;

	return sportcar;
}
