#include "Container.h"

list* create_base(list* base) {
	return create_list(base);
}

void add_element(list* base, item* element) {
	add_item(base, element);
}

item* deserialize_element(int type, char* str) {
	item* new_element = (item*)malloc(sizeof(item));
	Bike* new_bike = (Bike*)malloc(sizeof(Bike));
	Bicycle* new_bicycle = (Bicycle*)malloc(sizeof(Bicycle));
	Car* new_car = (Car*)malloc(sizeof(Car));
	Sportcar* new_sportcar = (Sportcar*)malloc(sizeof(Sportcar));
	Scooter* new_scooter = (Scooter*)malloc(sizeof(Scooter));

	switch (type) {
	case BIKE_ID:
		new_bike = deserialize_bike(str);
		new_element->data = new_bike;
		break;

	case BICYCLE_ID:
		new_bicycle = deserialize_bicycle(str);
		new_element->data = new_bicycle;
		break;

	case CAR_ID:
		new_car = deserialize_car(str);
		new_element->data = new_car;
		break;

	case SPORTCAR_ID:
		new_sportcar = deserialize_sportcar(str);
		new_element->data = new_sportcar;
		break;

	case SCOOTER_ID:
		new_scooter = deserialize_scooter(str);
		new_element->data = new_scooter;
		break;
	}

	new_element->type = type;
	
	return new_element;
}

char* serialize_element(item* element) {
	switch (element->type) {
	case BIKE_ID:
		return serialize_bike(element->data);

	case BICYCLE_ID:
		return serialize_bicycle(element->data);

	case CAR_ID:
		return serialize_car(element->data);

	case SPORTCAR_ID:
		return serialize_sportcar(element->data);

	case SCOOTER_ID:
		return serialize_scooter(element->data);
	}

	return 0;
}

void show_base(list* base) {
	item* element = base->head;
	if (element == NULL) {
		printf("base is empty");
		return;
	}

	char* str = (char*)calloc(sizeof(char), 256);
	while (element != NULL) {
		printf("%d. %d\n%s\n", element->id, element->type, serialize_element(element));
		element = element->next;
	}
}

void save_base(list* base) {
	char filename[256];
	strcpy(filename, "Bases/");
	strcat(filename, base->name);
	strcat(filename, ".txt");
	FILE* file = fopen(filename, "w");

	item* element = base->head;
	while (element != NULL) {
		fprintf(file, "%d\n%s\n", element->type, serialize_element(element));
		element = element->next;
	}
	fclose(file);
}

void load_base(list* base) {
	char filename[256];
	strcpy(filename, "Bases/");
	strcat(filename, base->name);
	strcat(filename, ".txt");
	FILE* file = fopen(filename, "r");

	if (!file) {
		fclose(file);
		return;
	}

	item* element = (item*)malloc(sizeof(item));
	char* str;
	int type;
	while (!feof(file)) {
		fscanf(file, "%d\n", &type);
		str = (char*)calloc(sizeof(char), 256);
		str = fgets(str, 256, file);
		element = deserialize_element(type, str);
		add_element_to_the_end(base, element);
	}
	fclose(file);
}

list* get_data(list* base) {
	list* data_list = (list*)malloc(sizeof(list));
	data_list = create_base(data_list);
	data_list->name = base->name;
	item* buf = (item*)malloc(sizeof(item));
	buf->id = base->head->id;
	buf->type = base->head->type;
	buf->data = base->head->data;
	buf->next = base->head->next;
	
	while (buf != NULL && buf->next != NULL) {
		add_element_to_the_end(data_list, buf);
		buf->id = buf->next->id;
		buf->type = buf->next->type;
		buf->data = buf->next->data;
		buf->next = buf->next->next;
	}
	if (buf != NULL) {
		add_element_to_the_end(data_list, buf);
	}

	return data_list;
}

void clean_base(list* base) {
	clean_list(base);
}

void free_base(list* base) {
	delete_list(base);
}

item* find_element_by_id(list* base, int id) {
	return find_item(base, id);
}

list* find_elements_by_type(list* base, int type) {
	return find_by_type(base, type);
}

void add_element_to_the_end(list* base, item* element) {
	add_to_the_end(base, element);
}

void delete_element(list* base, int id) {
	remove_item(base, id);
}
