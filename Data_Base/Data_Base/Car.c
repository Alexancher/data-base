#include "Car.h"

char* serialize_car(Car* car) {
	null_check(car);

	char* str = (char*)calloc(sizeof(char), 256);
	sprintf(str, "%s %s", car->brand, car->model);

	return str;
}

Car* deserialize_car(char* str) {
	null_check(str);

	Car* car = (Car*)malloc(sizeof(Car));
	int i = 0;
	int len = strlen(str);
	char* brand = (char*)calloc(sizeof(char), 256);
	char* model = (char*)calloc(sizeof(char), 256);

	while (i < len && str[i] != 32) {
		brand[i] = str[i];
		i++;
	}
	
	i++;
	int idx = 0;
	while (str[i] != '\n' && i < len) {
		model[idx] = str[i];
		idx++;
		i++;
	}

	car->brand = brand;
	car->model = model;

	return car;
}
