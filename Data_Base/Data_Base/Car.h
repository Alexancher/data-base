#pragma once

#include "Types.h"

#define CAR_ID 03

typedef struct Car {
	char* brand;
	char* model;
} Car;

char* serialize_car(Car* car);

Car* deserialize_car(char* str);
