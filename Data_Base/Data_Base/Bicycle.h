#pragma once

#include "Types.h"

#define BICYCLE_ID 02

typedef struct Bicycle {
	char* name;
	int wheels;
} Bicycle;

char* serialize_bicycle(Bicycle* bicycle);

Bicycle* deserialize_bicycle(char* str);
