#pragma once

#include "Types.h"

#define SCOOTER_ID 05

typedef struct Scooter {
	int size;
	int price;
} Scooter;

char* serialize_scooter(Scooter* scooter);

Scooter* deserialize_scooter(char* str);
